--[[
candidats will be in the following form in th etable candidats.candidats :

{
 [idofcandidat] = {name=('name'), branch = (branchnameid), governorat = (governoratid), marks = {["MG"] = (averagemark),
                    ["M"] = (markinmath), ["P"]=(markinphy)...}, score=generalscore,
                    wishes = {first_college_id, second_college_id, third_college_id}},
}
branchnameid would be :
  1 for "math" - abrv : M
  2 for "science" - abrv : SC
  3 for "technology" - abrv : TCH
  4 for "economy" - abrv : EC
  5 for "literature" - abrv : L
  6 for "computer science" - abrv : INF

governoratid would be :
  1 for TUNIS
  2 for SFAX
  3 for SOUSSE
  4 for KAIROUAN
  5 for SIDIBOUZID
  6 for GAFSA
  7 for KASSERINE
  8 for MEDENINE
  9 for KEBILI
  10 for KEF
  11 for BEJA
  12 for JENDOUBA
  13 for MAHDIA
  14 for NABEUL
  15 for MANNOUBA
  16 for MONASTIR
  17 for TOZEUR
  18 for BENAROUS
  19 for ARIANA
  20 for BIZERTE
  21 for SILIANA
  22 for GABES
  23 for TATAOUINE
  24 for ZAGHOUAN

marks are as follows :
  MG : Average mark
  M : Mathematics
  SP : Physics
  SVT : Science
  PH : Philosophy
  F : French
  A : Arabic
  ANG : English
  ALGO : Algorithms
  TIC : Internet and Communication Technology
  BD : Data Bases
  HG : Geography and History
  GE : Management
  EC : Economy
  ALL : German
  ESP : Spanish
  IT : Italian
  EP : Physical Education (Sports basically)
  INFO : Computer Science

colleges should be stored basically in a format as follows number of places,
]]--

-- USED LIBRARIES --
-- local tableutils = require 'tableutils'
-- END USED LIBRARIES --

-- BASICALLY CONSTANTS --
local __ID_TO_BRANCH = {'M','SC','TCH','EC','L','INF'};
local __ID_TO_GOVERNORAT = {'TUNIS','SFAX','SOUSSE','KAIROUAN','SIDIBOUZID','GAFSA','KASSERINE','MEDEINE','KEBILI',
	'KEF','BEJA','JENDOUBA','MAHDIA','NABEUL','MANNOUBA','MONASTIR','TOZEUR','BENAROUS','ARIANA','BIZERTE','SILIANA',
	'GABES','TATAOUINE','ZAGHOUAN'};
local __ID_TO_BRANCH_VERBOSE={'Mathematics','Practical Science','Technology','Economy','Literature','Computer Science'};
local __BRANCH_TO_ID = {M=1,SC=2,TCH=3,EC=4,L=5,INF=6};
local __GOVERNORAT_TO_ID = { TUNIS = 1, SFAX = 2, SOUSSE = 3, KAIROUAN = 4, SIDIBOUZID = 5,
  GAFSA = 6, KASSERINE = 7, MEDENINE = 8, KEBILI = 9, KEF = 10, BEJA = 11, JENDOUBA = 12,MAHDIA = 13, NABEUL = 14,
  MANNOUBA = 15, MONASTIR = 16, TOZEUR = 17, BENAROUS = 18, ARIANA = 19, BIZERTE = 20, SILIANA = 21, GABES = 22,
  TATAOUINE = 23, ZAGHOUAN = 24};
local __SUBJECT_TO_SUBJECT_VERBOSE = {MG='Average Mark', M = 'Mathematics', SP = 'Physics', SVT = 'Science',
    PH = 'Philosophy', F = 'Frech', ANG = 'English', ALGO = 'Algorithms', TIC = 'Internet&Communication Technology',
    BD = 'Data Bases', HG = 'Geography and History', GE = 'Management', EC = 'Economy', ALL = 'German', ESP = 'Spanish',
    IT = 'Italian', EP = 'Physical Education', A = 'Arabic', INFO = 'Computer Science'};
-- END OF BASICALLY CONSTANTS
local __SPECIALFORMULA_TO_FUNCTION_CACHE = {}

local orientation = {}
orientation.colleges = {}
orientation.candidats = {}

local function calculate_general_score(branchid, marks)
  if branchid == 1 then
    return 4*marks.MG + 2*marks.M + 1.5*marks.SP + 0.5*marks.SVT + 1*marks.F + 1*marks.ANG;
  elseif branchid == 2 then
    return 4*marks.MG + 1*marks.M + 1.5*marks.SP + 1.5*marks.SVT + 1*marks.F + 1*marks.ANG;
  elseif branchid == 3 then
    return 4*marks.MG + 1.5*marks.TE + 1.5*marks.M + 1*marks.SP + 1*marks.F + 1*marks.ANG;
  elseif branchid == 4 then
    return 4*marks.MG + 1.5*marks.A + 1.5*marks.PH + 1*marks.HG + 1*marks.F + 1*marks.ANG;
  elseif branchid == 5 then
    return 4*marks.MG + 1.5*marks.EC + 1.5*marks.GE + 0.5*marks.M + 0.5*marks.HG + 1*marks.F + 1*marks.ANG;
  elseif branchid == 6 then
    return 4*marks.MG + 1.5*marks.M + 1.5*marks.ALGO + 0.5*marks.SP + 0.25*(marks.TIC+marks.BD) + 1*marks.F+1*marks.ANG;
  end
end

function orientation:add_candidat(name, branchname, marks, wishes, governorat) -- district contains where he comes from
  -- name should just be the name of the person
  -- branch should be part of __ID_TO_BRANCH
  -- marks should be a string with this format : 'SUBJ=MARK;SUBJ=MARK;...'
	-- wishes is a table with college ids {wish1, wish2,...} ex : {110251, 11215}
  local nid = #self.candidats + 1; -- candidat ID
  local cnd = {}; -- table containing the new candidat info

  cnd.name = name;
	cnd.governorat = __GOVERNORAT_TO_ID[string.upper(governorat)]
  cnd.branch =__BRANCH_TO_ID[string.upper(branchname)] or error('Branch name abrv \''..branchname..'\' is incorrect',2);
  cnd.marks = {}
	cnd.wishes = {}
	cnd.result = nil -- In here we will store the result of the candidat's orientation
  for SUBJ, MARK in marks:gmatch('(%u+)%s*=%s*([%d%.,]+)') do
    if not __SUBJECT_TO_SUBJECT_VERBOSE[string.upper(SUBJ)] then
      error('subject initials '..SUBJ..' are not recognized', 2);
    end
		SUBJ = string.upper(SUBJ);
    MARK = MARK:gsub(',', '.');
    cnd.marks[SUBJ] = tonumber(MARK);
  end

	cnd.score = calculate_general_score(cnd.branch, cnd.marks)

	for i,v in ipairs(wishes) do
		assert(self.colleges[v]~=nil, 'The college id '..v..' of the wish n°'..i..' does not exist')
		assert(self.colleges[v].branches[cnd.branch]~=nil,
			'The college id '..v..' of the wish n°'..i..' doesn\'t accept '..__ID_TO_BRANCH[cnd.branch]..' students.')
		cnd.wishes[#cnd.wishes+1]={
			id=v,
			score=cnd.score+self.colleges[v].branches[cnd.branch].specialformula(cnd.marks,governorat)
		}
	end

	self.candidats[nid] = cnd;
end

local function special_formula_to_function(specialformula)
	return load('return function(marks)return '..string.gsub(specialformula,'(%a+)','marks.%1')..';end')();
end

function orientation:add_college(id,name,nameabreviation,branches,governorat_advantage)--id of college,eg 115001
  -- Branches is a table {} like this :
  -- {
  --  [branch1] = {capacity=number, specialformula = specialformula, accepted={}}
  --  ...
  -- }
  -- Note : branch1 is the ID of the branch (1 2 3 ...)
	-- governorat_advantage will be either true or false
	-- Note : specialformula will be given as a string with the subjects in capital letters
	-- eg : (M+SP+SVT)/3
	-- eg2: PH
	assert(self.colleges[id]==nil, 'College with id '..id..' already exists');
	self.colleges[id] = {name=name, abrv = nameabreviation,
		branches=branches,governorat_advantage=governorat_advantage or false};

	for _,v in pairs(self.colleges[id].branches) do
		if __SPECIALFORMULA_TO_FUNCTION_CACHE[v.specialformula] then
			v.specialformula = __SPECIALFORMULA_TO_FUNCTION_CACHE[v.specialformula];
		else
			__SPECIALFORMULA_TO_FUNCTION_CACHE[v.specialformula] = special_formula_to_function(v.specialformula);
			v.specialformula = __SPECIALFORMULA_TO_FUNCTION_CACHE[v.specialformula];
		end
		v.accepted = {}
	end
end

function orientation:run() -- this will do the orientation in order.
	-- First get the list of all candidats into buckets seperated by their branches
	local score_sorter = function(candA, candB)
		return candA[2]>candB[2];
	end

	local cands = {{},{},{},{},{},{}}
	for i,v in ipairs(self.candidats) do
		cands[v.branch][#cands[v.branch]+1]=i -- list them by their id.
	end

	-- Now run the whole orientation branch by branch

	for i=1,6 do -- There are 6 branches.
		-- Now get the id of the most demanded college from the current candidats
		local colleges = {};
		local cndcid;
		for _,v in ipairs(cands[i]) do
			cndcid = self.candidats[cands[i][v]].wishes[1].id;
			if self.colleges[cndcid].branches[i].capacity > 0 then -- Make sure college still has places in it.
				colleges[cndcid] = colleges[cndcid] and colleges[cndcid] + 1 or 1;
			end
		end
		local highest_id, highest_count = cndcid, colleges[cndcid]
		for k,v in pairs(colleges) do
			if v>highest_count then
				highest_id, highest_count = k, v
			end
		end

		-- Now we know the college we're working for (highest_id)
		-- We'll get the candidats who wanted it first.
		-- And we'll add them to the college until the capacity reaches 0.

		local to_sort_through = {}
		for _,v in ipairs(cands[i]) do
			cndcid = self.candidats[cands[i][v]].wishes[1].id;
			if cndcid == highest_id then
				to_sort_through[#to_sort_through] = {cands[i], self.candidats[cands[i]].wishes[1].score}
			end
		end

	end
end

-------------------------------------------
------------#################--------------
------------####CHILLZONE####--------------
------------#################--------------
-------------------------------------------

--TESTZONE--
------------

-- Adding colleges
orientation:add_college(1, 'INSAT', 'INSAT',
{[1]={capacity=3, specialformula='(M+SP+INFO)/3'}, [2]={capacity=1, specialformula='(M+SP)/2'}});

orientation:add_college(2, 'ISKAE', 'ISKAE',
{[1]={capacity=2, specialformula='(M+SP)/2'}, [2]={capacity=1, specialformula='M'}}, true);

orientation:add_college(3, 'POSTR', 'POSTR',
{[1]={capacity=1, specialformula='(SP+INFO)/2'}}, true);

orientation:add_college(4, 'RMZLN', 'RMZLN',
{[1]={capacity=3, specialformula='SP'}});

orientation:add_college(5, 'LDOZN', 'LDOZN',
{[1]={capacity=2, specialformula='(A+ANG+F)/3'},[2]={capacity=2, specialformula='(A+ANG+F)/3'}});

-- Adding candidats
orientation:add_candidat('Moumin BENABDALLAH', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'NABEUL');
orientation:add_candidat('Salim ELMNEDFI', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Eya LAOUINI', 'm',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,3,2,4,5},'TUNIS');
orientation:add_candidat('Sami ATTAR', 'm',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,3,2,4,5},'NABEUL');
orientation:add_candidat('Chahinez TAMROUT', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Jesir ELMANNENI', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Hajer ELBALDI', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Rami JRIDI', 'm',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,3,2,4,5},'NABEUL');
orientation:add_candidat('Hazem ABOUSHANAB', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Oussema JARJAR', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'TUNIS');
orientation:add_candidat('Ali ELSAMEEH', 'sc',
'MG=15.13 SVT=15.75 M=13,25 SP=12.75 ANG=16.5 F=17 A=12.5 INFO=19.75',
{1,2,5},'NABEUL');

print("There are "..#orientation.candidats.." candidats")
print("There are "..#orientation.colleges.." colleges")
