local tableutils = {}

function tableutils.find(tbl, val) -- Returns true/false
  for _,v in ipairs(tbl) do
    if v == val then
      return true;
    end
  end
  return false;
end

return tableutils;
